# environment
```
install nodejs 12 LTS
```

# FIX version
FIX.4.4

# question
```
1) From the file, please extract the executed transactions and output a CSV file with the
following information:
 Stock Code
 Transaction Quantity
 Transaction Price
 Transaction Side (Buy/Sell)
 Account
 Transaction Reference ID
 Transaction Time
We prefer you to use C#, Python or PowerShell for this task. But feel free to use any other
programming or scripting languages you see fit and explain to us your decisions. Please submit
the source files and output CSV file.
2) What were the orders that the account sent? And what are the account’s positions now?
3) The client has left you a message that “something seems wrong” and she is in meeting for the
rest of the day. Could you investigate and see what might be wrong in her eyes? How would
you proceed the investigation?
Please contact us if you need any clarifications.
```

# answer
## question 1
### get the csv file
```
npm install
npm run start > out.json
npx json2csv -i ./out.json -o out.csv
```
#### header mapping
```
"action" = Action 
"stock_code" = Stock Code
"quantity" = Transaction Quantity
"side" = Transaction Price
"order_status" = Transaction status
"price" = Transaction Price
"account" = Account
"reference_id" = Transaction Reference ID
"time" = Transaction Time UTC
"message_type" = Extra Message
```
## question 2
It is a order for `buying` stock from the market. Most of them is partially completed. And I think it is a `long positions` account,as it is owning stocks from market and I cannot see the selling record based on the log.

## question 3
After I review the log, I guess that client have `issue with the login and logout` system. As I can see the login and logout are not existence as a pair and the logout action keep appearing after the first login. She might facing a login or logout issue.

# reference
1. https://www.onixs.biz/fix-dictionary/4.2/fields_by_name.html
1. https://gitlab.com/logotype/fixparser/-/tree/master
1. https://www.npmjs.com/package/json2csv
1. https://github.com/connamara/logstash-filter-fix_protocol

# improvement?
ping me via `wck.alpha@gmail.com`
