import FIXParser from 'fixparser'
import fs from 'fs'

const fixParser = new FIXParser.default()
const rawFile = fs.readFileSync('./FIX.09-Jan-2018.log', { encoding: 'utf8', flag: 'r' })
const messages = fixParser.parse(rawFile)
const jsonFile = messages.filter(message => (message.getBriefDescription().toLowerCase() !== 'Heartbeat'.toLowerCase()))
  .map((message) => {
    const emptyMessage = {
      action: '',
      stock_code: '',
      quantity: '',
      side: '',
      order_status: '',
      price: '',
      account: '',
      reference_id: '',
      time: '',
      message_type: ''
    }
    const messageWithdata = {
      ...emptyMessage,
      reference_id: message.getField(FIXParser.Fields.OrderID) ? message.getField(FIXParser.Fields.OrderID).value : '',
      action: message.getBriefDescription(),
      stock_code: message.getField(FIXParser.Fields.Symbol) ? message.getField(FIXParser.Fields.Symbol).value : '',
      account: message.getField(FIXParser.Fields.Account) ? message.getField(FIXParser.Fields.Account).value : '',
      quantity: message.getField(FIXParser.Fields.OrderQty) ? message.getField(FIXParser.Fields.OrderQty).value : '',
      price: message.getField(FIXParser.Fields.LastPx) ? message.getField(FIXParser.Fields.LastPx).value.toFixed(2) : '',
      side: message.getField(FIXParser.Fields.Side) ? message.getField(FIXParser.Fields.Side).enumeration.description : '',
      order_status: message.getField(FIXParser.Fields.OrdStatus) ? message.getField(FIXParser.Fields.OrdStatus).enumeration.description : '',
      message_type: message.getField(FIXParser.Fields.MsgType) ? message.getField(FIXParser.Fields.MsgType).enumeration.elaboration : '',
      time: message.getField(FIXParser.Fields.TransactTime) ? message.getField(FIXParser.Fields.TransactTime).value : ''
    }
    return messageWithdata
  })
console.log(JSON.stringify(jsonFile, '', '  '))
